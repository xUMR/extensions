#!/usr/bin/python

# read version info from pom.xml, update readme.md

import re
import os
import shutil

def replaceInFile(file, pattern, sub):
    tmp = file + ".tmp"
    shutil.move(file, tmp)
    with open(file, "w") as toWrite, open(tmp, "r") as toRead:
        for line in toRead:
            toWrite.write(re.sub(pattern, sub, line))
    os.remove(tmp)

def findFirstInFile(file, pattern):
    m = None
    with open(file, "r") as toRead:
        for line in toRead:
            m = re.search(pattern, line, re.M)
            if m is None:
                continue
            if m.lastindex > 0:
                m = m.group(1)
                break
    return m

def main():
    version = findFirstInFile("pom.xml", "<version>(.*)</version>")
    print "version: " + str(version)
    if version is None:
        return
    replaceInFile("readme.md", "<version>(.*)</version>", "<version>" + version + "</version>")

if __name__ == "__main__":
    main()
