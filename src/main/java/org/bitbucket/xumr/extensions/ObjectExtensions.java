package org.bitbucket.xumr.extensions;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.bitbucket.xumr.extensions.MethodExtensions.invoke;
import static org.bitbucket.xumr.extensions.StringExtensions.firstToLower;

/**
 * @author umur
 */
public final class ObjectExtensions {
    private ObjectExtensions() {}

    private static String stripPropertyPrefix(String property) {
        if (property.startsWith("get")) return firstToLower(property.substring(3));
        if (property.startsWith("set")) return firstToLower(property.substring(3));
        if (property.startsWith("is")) return firstToLower(property.substring(2));
        throw new RuntimeException("Unknown property prefix.");
    }

    private static <T> PropertyDescriptor[] getDescriptors(T obj) throws IntrospectionException {
        return Introspector.getBeanInfo(obj.getClass()).getPropertyDescriptors();
    }

    /**
     * Returns fields of an object which are exposed by getters
     */
    public static <T> Map<String, Object> properties(T obj) {

        List<Method> getters = new ArrayList<>();
        try {
            PropertyDescriptor[] descriptors = getDescriptors(obj);
            for (PropertyDescriptor descriptor : descriptors) {
                if (descriptor.getReadMethod() != null) {
                    getters.add(descriptor.getReadMethod());
                }
            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }

        Map<String, Object> properties = new HashMap<>(getters.size() - 1);

        getters.forEach(getter -> {
            String name = getter.getName();
            if (!name.equals("getClass")) {
                properties.put(stripPropertyPrefix(name), invoke(getter, obj));
            }
        });
        return properties;
    }

    /**
     * Populates an object's fields via its setters using a map object. Returns the given object.
     */
    public static <T> T populate(Map<String, Object> properties, T obj) {

        List<Method> setters = new ArrayList<>();
        try {
            PropertyDescriptor[] descriptors = getDescriptors(obj);
            for (PropertyDescriptor descriptor : descriptors) {
                if (descriptor.getWriteMethod() != null) {
                    setters.add(descriptor.getWriteMethod());
                }
            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }

        try {
            for (Method setter : setters) {
                Class type = setter.getParameterTypes()[0];
                Object property = properties.get(stripPropertyPrefix(setter.getName()));

                if (property instanceof String && !type.equals(String.class)) {
                    setter.invoke(obj, StringExtensions.parse((String)property, type));
                }
                else {
                    setter.invoke(obj, property);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
