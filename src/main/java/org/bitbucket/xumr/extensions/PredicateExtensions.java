package org.bitbucket.xumr.extensions;

import java.util.function.Predicate;

/**
 * @see Predicate
 * @author umur
 */
@SuppressWarnings("unused")
public final class PredicateExtensions {
    private PredicateExtensions() {}

    public static <T> Predicate<T> or(Predicate<T> predicate, Predicate<T> other, Predicate<T>... predicates) {
        predicate = predicate.or(other);
        for (Predicate<T> tPredicate : predicates) {
            predicate = predicate.or(tPredicate);
        }
        return predicate;
    }

    public static <T> Predicate<T> and(Predicate<T> predicate, Predicate<T> other, Predicate<T>... predicates) {
        predicate = predicate.and(other);
        for (Predicate<T> tPredicate : predicates) {
            predicate = predicate.and(tPredicate);
        }
        return predicate;
    }
}
