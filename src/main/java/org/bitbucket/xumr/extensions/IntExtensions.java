package org.bitbucket.xumr.extensions;

/**
 * @author umur
 */
public final class IntExtensions {
    private IntExtensions() {}

    private static final int[] powersOf10 = {
//          0  1   2    3     4      5       6        7         8          9
            1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
    };

    public static int digitCount(int num) {
        int d = powersOf10[1];
        int compare = powersOf10[9];

        for (; compare > 0; compare /= 10) {
            if (num > compare) {
                return d;
            }
            d--;
        }
        return d;
    }

    public static int clamp(int value, int min, int max) {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }

    /**
     * Returns x^n, n must be positive.
     */
    public static int power(int x, int n) {
        n = clamp(0, n, n);

        if (x == 10 && n < 9) return powersOf10[n];

        if (n == 0) return 1;
        if (n == 1) return n;

        int power = 1;
        for (int i = 0; i < n; i++) {
            power *= n;
        }
        return power;
    }

    /**
     * Returns true if n equals one of ints.
     */
    public static boolean any(int n, int... ints) {
        for (int i : ints) {
            if (i == n) {
                return true;
            }
        }
        return false;
    }
}
