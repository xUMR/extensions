package org.bitbucket.xumr.extensions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;

/**
 * @see Method
 * @author umur
 */
public final class MethodExtensions {
    private MethodExtensions() {}
    /**
     * Wraps Method.invoke method in try/catch, may throw {@link RuntimeException}.
     * @see Method
     */
    public static Object invoke(Method method, Object object, Object... args) throws RuntimeException {
        try {
            return method.invoke(object, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
