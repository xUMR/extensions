package org.bitbucket.xumr.extensions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;

/**
 * @see String
 * @author umur
 */
@SuppressWarnings("unused")
public final class StringExtensions {

    private StringExtensions() {}

    public static final String EMPTY = "";
    public static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String DIGIT = "0123456789";
    public static final String HEX = "0123456789ABCDEF";

    private static final Map<Class, Function<String, ?>> parsers = new HashMap<>();

    static {
        parsers.put(int.class, Integer::parseInt);
        parsers.put(Integer.class, Integer::parseInt);
        parsers.put(long.class, Long::parseLong);
        parsers.put(Long.class, Long::parseLong);
        parsers.put(short.class, Short::parseShort);
        parsers.put(Short.class, Short::parseShort);
        parsers.put(byte.class, Byte::parseByte);
        parsers.put(Byte.class, Byte::parseByte);
        parsers.put(boolean.class, Boolean::parseBoolean);
        parsers.put(Boolean.class, Boolean::parseBoolean);
        parsers.put(float.class, Float::parseFloat);
        parsers.put(Float.class, Float::parseFloat);
        parsers.put(double.class, Double::parseDouble);
        parsers.put(Double.class, Double::parseDouble);

        parsers.put(BigInteger.class, BigInteger::new);
        parsers.put(BigDecimal.class, BigDecimal::new);

        parsers.put(java.time.Duration.class, java.time.Duration::parse);
        parsers.put(java.time.Instant.class, java.time.Instant::parse);
        parsers.put(java.time.LocalDate.class, java.time.LocalDate::parse);
        parsers.put(java.time.LocalDateTime.class, java.time.LocalDateTime::parse);
        parsers.put(java.time.LocalTime.class, java.time.LocalTime::parse);
        parsers.put(java.time.MonthDay.class, java.time.MonthDay::parse);
        parsers.put(java.time.OffsetDateTime.class, java.time.OffsetDateTime::parse);
        parsers.put(java.time.OffsetTime.class, java.time.OffsetTime::parse);
        parsers.put(java.time.Period.class, java.time.Period::parse);
        parsers.put(java.time.Year.class, java.time.Year::parse);
        parsers.put(java.time.YearMonth.class, java.time.YearMonth::parse);
        parsers.put(java.time.ZonedDateTime.class, java.time.ZonedDateTime::parse);

        parsers.put(java.util.Date.class, java.util.Date::parse);
        parsers.put(java.sql.Date.class, java.sql.Date::parse);
        parsers.put(java.sql.Time.class, java.sql.Time::parse);
        parsers.put(java.sql.Timestamp.class, java.sql.Timestamp::parse);
    }

    public static void addParser(Class<?> type, Function<String, ?> parser) {
        parsers.put(type, parser);
    }

    public static Function<String, ?> removeParserFor(Class<?> type) {
        return parsers.remove(type);
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    public static int[] digitsOf(String s) {
        s = s.trim();

        int[] digits = new int[s.length()];
        char[] chars = s.toCharArray();

        for (int i = 0; i < s.length(); i++) {
            char c = chars[i];
            int value = Character.getNumericValue(c);
            if (value < 0 || value > 9) {
                return ArrayExtensions.Empty.ints;
            }
            digits[i] = value;
        }

        return digits;
    }

    public static Iterable<Character> iterable(String s) {
        Collection<Character> chars = new ArrayList<>();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            chars.add(s.charAt(i));
        }
        return chars;
    }

    /**
     * Returns true if str equals any of the strings.
     */
    public static boolean in(String str, String... strings) {
        for (String string : strings) {
            if (string.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a random character from the given string.
     */
    public static char randomChar(String s, Random random) {
        return s.charAt(random.nextInt(s.length()));
    }

    /**
     * Returns a string of random characters from the given string.
     */
    public static String randomString(int length, String s, Random random) {
        StringBuilder sb = new StringBuilder();
        int options = s.length();
        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(options));
        }
        return sb.toString();
    }

    /**
     * Lowercases the first letter.
     * <i>FirstName</i> -> <i>firstName</i>
     */
    public static String firstToLower(String string) {
        return string.toLowerCase().substring(0, 1) + string.substring(1);
    }

    /**
     * Lowercases the first letter using the given locale.
     * <i>FirstName</i> -> <i>firstName</i>
     */
    public static String firstToLower(String string, Locale locale) {
        return string.toLowerCase(locale).substring(0, 1) + string.substring(1);
    }

    /**
     * Capitalises the first letter.
     * <i>firstName</i> -> <i>FirstName</i>
     */
    public static String firstToUpper(String string) {
        return string.toUpperCase().substring(0, 1) + string.substring(1);
    }

    /**
     * Capitalises the first letter using the given locale.
     * <i>firstName</i> -> <i>FirstName</i>
     */
    public static String firstToUpper(String string, Locale locale) {
        return string.toUpperCase(locale).substring(0, 1) + string.substring(1);
    }

    /**
     * Returns the given string for unknown types. Parses the following types:
     * <ul>
     *     <li>Primitive types</li>
     *     <li>Boxed types</li>
     *     <li>Arbitrary-Precision types</li>
     *     <li>java.util.Date and java.sql types</li>
     *     <li>java.time (JSR-310) types</li>
     * </ul>
     *
     * @see java.time
     */
    public static Object parse(String str, Class type) {
        Function<String, ?> parser = parsers.get(type);
        if (parser == null) {
            return str;
        }
        return parser.apply(str);
    }
}
