package org.bitbucket.xumr.extensions;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.*;

/**
 * @see Arrays
 * @author umur
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class ArrayExtensions {
    private ArrayExtensions() {}

    /**
     * Container for empty arrays.
     */
    public static final class Empty {
        public static final int[] ints = new int[0];
        public static final char[] chars = new char[0];
        public static final byte[] bytes = new byte[0];
        public static final long[] longs = new long[0];
        public static final short[] shorts = new short[0];
        public static final float[] floats = new float[0];
        public static final double[] doubles = new double[0];
        public static final boolean[] booleans = new boolean[0];
    }

    /**
     *
     */
    public static final class New {
        private static final Map<Class, Function<Integer, ?>> factory = new HashMap<>();

        static {
            factory.put(int.class, int[]::new);
            factory.put(Integer.class, int[]::new);
            factory.put(char.class, char[]::new);
            factory.put(Character.class, char[]::new);
            factory.put(byte.class, byte[]::new);
            factory.put(Byte.class, byte[]::new);
            factory.put(long.class, long[]::new);
            factory.put(Long.class, long[]::new);
            factory.put(short.class, short[]::new);
            factory.put(Short.class, short[]::new);
            factory.put(float.class, float[]::new);
            factory.put(Float.class, float[]::new);
            factory.put(double.class, double[]::new);
            factory.put(Double.class, double[]::new);
            factory.put(boolean.class, boolean[]::new);
            factory.put(Boolean.class, boolean[]::new);
        }

        public static Object create(Class primitive, int length) {
            Function<Integer, ?> generator = factory.get(primitive);
            return factory.get(primitive).apply(length);
        }
    }

    /**
     * Returns the index of the element if it is found, returns -1 otherwise.
     */
    public static <T> int linearSearch(T[] array, T e) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(e)) {
                return i;
            }
        }
        return -1;
    }

    public static <T> boolean isNullOrEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> void forEach(T[] array, Consumer<T> action) {
        for (T t : array) {
            action.accept(t);
        }
    }

    /**
     * Applies a filter to an array and returns a list.
     */
    public static <T> List<T> filter(T[] array, Predicate<T> predicate) {
        List<T> list = new ArrayList<>();
        for (T t : array) {
            if (predicate.test(t)) {
                list.add(t);
            }
        }
        return list;
    }

    /**
     * Maps an array to another.
     */
    @SuppressWarnings("unchecked")
    public static <T, E> E[] map(T[] array, Function<T, E> function, Class<E> cls) {
        E[] result = (E[])Array.newInstance(cls, array.length);

        for (int i = 0; i < array.length; i++) {
            result[i] = function.apply(array[i]);
        }
        return result;
    }

    /**
     * Maps an array to another, allows using of the loop index.
     */
    @SuppressWarnings("unchecked")
    public static <T, E> E[] map(T[] array, BiFunction<T, Integer, E> function, Class<E> cls) {
        E[] result = (E[])Array.newInstance(cls, array.length);

        for (int i = 0; i < array.length; i++) {
            result[i] = function.apply(array[i], i);
        }
        return result;
    }

    /**
     * Reduces an array to a single element.
     */
    public static <T> T reduce(T[] array, BiFunction<T, T, T> function, T identity) {
        T result = identity;
        for (T t : array) {
            result = function.apply(result, t);
        }
        return result;
    }

    /**
     * Returns true if any of the elements in an array satisfies the given predicate.
     */
    public static <T> boolean any(T[] array, Predicate<T> predicate) {
        for (T t : array) {
            if (predicate.test(t)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if all of the elements in an array satisfies the given predicate.
     */
    public static <T> boolean all(T[] array, Predicate<T> predicate) {
        for (T t : array) {
            if (!predicate.test(t)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the number of elements which satisfy the given predicate.
     */
    public static <T> int count(T[] array, Predicate<T> predicate) {
        int count = 0;
        for (T t : array) {
            if (predicate.test(t)) {
                count++;
            }
        }
        return count;
    }

    /**
     * Converts an array to a {@link HashSet}
     */
    public static <T> Set<T> asSet(T... elements) {
        Set<T> set = new HashSet<>();
        forEach(elements, set::add);
        return set;
    }

    /**
     * Wraps t in an array
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] single(T t) {
        T[] array = (T[])Array.newInstance(t.getClass(), 1);
        array[0] = t;
        return array;
    }

    public static int[] single(int n) {
        return new int[] { n };
    }

    public static String[] single(String n) {
        return new String[] { n };
    }

    public static <T> T randomElement(T[] array, Random random) {
        return array[random.nextInt(array.length)];
    }

    public static Object randomElement(Object array, Random random) {
        return Array.get(array, random.nextInt(Array.getLength(array)));
    }

    /**
     * Returns a subsequence of a typed-array.
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] subsequence(T[] array, int from, int to) {
        T[] sub = (T[])Array.newInstance(array[0].getClass(), to - from);
        System.arraycopy(array, from, sub, 0, to - from);

        return sub;
    }

    @SuppressWarnings("SuspiciousSystemArraycopy")
    private static Object sub(Object array, int from, int to) {
        Object sub = New.create(Array.get(array, 0).getClass(), to - from);
        System.arraycopy(array, from, sub, 0, to - from);

        return sub;
    }

    public static int[] subsequence(int[] array, int from, int to) {
        return (int[])sub(array, from, to);
    }

    public static char[] subsequence(char[] array, int from, int to) {
        return (char[])sub(array, from, to);
    }

    public static byte[] subsequence(byte[] array, int from, int to) {
        return (byte[])sub(array, from, to);
    }

    public static long[] subsequence(long[] array, int from, int to) {
        return (long[])sub(array, from, to);
    }

    public static short[] subsequence(short[] array, int from, int to) {
        return (short[])sub(array, from, to);
    }

    public static float[] subsequence(float[] array, int from, int to) {
        return (float[])sub(array, from, to);
    }

    public static double[] subsequence(double[] array, int from, int to) {
        return (double[])sub(array, from, to);
    }

    public static boolean[] subsequence(boolean[] array, int from, int to) {
        return (boolean[])sub(array, from, to);
    }
}
