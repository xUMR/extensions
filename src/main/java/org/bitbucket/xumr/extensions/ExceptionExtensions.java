package org.bitbucket.xumr.extensions;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @see Exception
 * @author umur
 */
@SuppressWarnings("unused")
public final class ExceptionExtensions {

    private ExceptionExtensions() {}

    public static String stackTrace(final Exception e) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        e.printStackTrace(pw);

        String result = sw.toString();

        pw.close();
        // no need to close StringWriter according to javadoc

        return result;
    }
}

