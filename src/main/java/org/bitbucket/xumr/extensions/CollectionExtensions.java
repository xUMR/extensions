package org.bitbucket.xumr.extensions;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @see Collection
 * @see Collections
 * @see Function
 * @see BiFunction
 * @see Predicate
 * @author umur
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class CollectionExtensions {

    private CollectionExtensions() {}

    public static boolean isNullOrEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Filters a collection into an {@link ArrayList}.
     */
    public static <T> Collection<T> filter(Collection<T> collection, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t : collection) {
            if (predicate.test(t))
                result.add(t);
        }
        return result;
    }

    /**
     * Filters a collection into the given {@link Collection}.
     */
    public static <T> Collection<T> filter(Collection<T> collection, Predicate<T> predicate, Collection<T> target) {
        for (T t : collection) {
            if (predicate.test(t))
                target.add(t);
        }
        return target;
    }

    /**
     * Maps a collection to an {@link ArrayList}.
     */
    public static <T, E> List<E> map(Collection<T> collection, Function<T, E> function) {
        List<E> result = new ArrayList<>();
        for (T t : collection) {
            result.add(function.apply(t));
        }
        return result;
    }

    /**
     * Maps a collection to an {@link ArrayList}, allows using of the loop index.
     */
    public static <T, E> List<E> map(Collection<T> collection, BiFunction<T, Integer, E> function) {
        List<E> result = new ArrayList<>();
        int i = 0;
        for (T t : collection) {
            result.add(function.apply(t, i++));
        }
        return result;
    }

    /**
     * Maps a collection to a {@link HashMap}
     */
    public static <T, K, V> Map<K, V> mapMap(Collection<T> collection, Function<T, Map.Entry<K, V>> function) {
        Map<K, V> map = new HashMap<>();
        for (T t : collection) {
            Map.Entry<K, V> pair = function.apply(t);
            map.put(pair.getKey(), pair.getValue());
        }
        return map;
    }

    /**
     * Maps a collection to a {@link HashSet}.
     */
    public static <T, E> Set<E> mapSet(Collection<T> collection, Function<T, E> function) {
        Set<E> result = new HashSet<E>();
        for (T t : collection) {
            result.add(function.apply(t));
        }
        return result;
    }

    /**
     * Maps a collection to the given collection. Returns the collection in order to allow chaining.
     */
    public static <T, E> Collection<E> map(Collection<T> collection, Function<T, E> function, Collection<E> target) {
        for (T t : collection) {
            target.add(function.apply(t));
        }
        return target;
    }

    /**
     * Reduces a collection into a single element.
     */
    public static <T, E> E reduce(Collection<T> collection, Function<Collection<T>, E> function) {
        return function.apply(collection);
    }

    /**
     * Reduces a collection into a single element. The given function must be commutative.
     */
    public static <T> T reduce(Collection<T> collection, BiFunction<T, T, T> function, T identity) {
        T result = identity;
        for (T t : collection) {
            result = function.apply(result, t);
        }
        return result;
    }

    /**
     * Returns true if any of the elements in a {@link Collection} satisfies the given {@link Predicate}.
     */
    public static <T> boolean any(Collection<T> collection, Predicate<T> predicate) {
        for (T t : collection) {
            if (predicate.test(t))
                return true;
        }
        return false;
    }

    /**
     * Returns true if all of the elements in a {@link Collection} satisfies the given {@link Predicate}.
     */
    public static <T> boolean all(Collection<T> collection, Predicate<T> predicate) {
        for (T t : collection) {
            if (!predicate.test(t))
                return false;
        }
        return true;
    }

    /**
     * Returns the number of elements which satisfy the given {@link Predicate}.
     */
    public static <T> int count(Collection<T> collection, Predicate<T> predicate) {
        int count = 0;
        for (T t : collection) {
            if (predicate.test(t))
                count++;
        }
        return count;
    }

    /**
     * Returns the first element in a collection.
     * Insertion order is not guaranteed in non-ordered collections.
     */
    public static <T> T first(Collection<T> collection) {
        if (isNullOrEmpty(collection)) return null;
        return collection.iterator().next();
    }

    /**
     * Returns the first element that satisfies the given predicate in a collection.
     * Insertion order is not guaranteed in non-ordered collections.
     */
    public static <T> T first(Collection<T> collection, Predicate<T> predicate) {
        if (isNullOrEmpty(collection)) return null;
        for (T t : collection) {
            if (predicate.test(t))
                return t;
        }
        return null;
    }

    /**
     * Returns the last element in a collection.
     * Insertion order is not guaranteed in non-ordered collections.
     */
    public static <T> T last(Collection<T> collection) {
        if (isNullOrEmpty(collection)) return null;

        int iterations = collection.size() - 1;
        Iterator<T> iterator = collection.iterator();
        for (int i = 0; i < iterations; i++) {
            iterator.next();
        }
        return iterator.next();
    }

    /**
     * Returns the last element that satisfies the given predicate.
     * Insertion order is not guaranteed in non-ordered collections.
     */
    public static <T> T last(Collection<T> collection, Predicate<T> predicate) {
        if (isNullOrEmpty(collection)) return null;

        T last = null;
        for (T t : collection) {
            if (predicate.test(t))
                last = t;
        }
        return last;
    }
}
