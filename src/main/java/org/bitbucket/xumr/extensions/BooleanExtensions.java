package org.bitbucket.xumr.extensions;

/**
 * @author umur
 */
@SuppressWarnings("unused")
public final class BooleanExtensions {
    private BooleanExtensions() {}

    /**
     * Returns true, only if b is true.
     */
    public static boolean isTrue(Boolean b) {
        return b != null && b;
    }

    /**
     * Returns true, only if b is false.
     */
    public static boolean isFalse(Boolean b) {
        return b != null && !b;
    }

    public static boolean or(boolean... bools) {
        boolean b = false;
        for (boolean bool : bools) b = b || bool;
        return b;
    }

    public static boolean and(boolean... bools) {
        boolean b = true;
        for (boolean bool : bools) b = b && bool;
        return b;
    }
}
