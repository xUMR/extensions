package org.bitbucket.xumr.extensions;

/**
 * @author umur
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class ComparableExtensions {
    private ComparableExtensions() {}

    /**
     * x &lt; y
     */
    public static <T> boolean lt(final Comparable<T> x, final T y) {
        return x.compareTo(y) < 0;
    }

    /**
     * x &gt; y
     */
    public static <T> boolean gt(final Comparable<T> x, final T y) {
        return x.compareTo(y) > 0;
    }

    /**
     * x = y
     */
    public static <T> boolean eq(final Comparable<T> x, final T y) {
        return x.compareTo(y) == 0;
    }

    /**
     * x ≠ y
     */
    public static <T> boolean ne(final Comparable<T> x, final T y) {
        return x.compareTo(y) != 0;
    }

    /**
     * x ≤ y
     */
    public static <T> boolean le(final Comparable<T> x, final T y) {
        return lt(x, y) || eq(x, y);
    }

    /**
     * x ≥ y
     */
    public static <T> boolean ge(final Comparable<T> x, final T y) {
        return gt(x, y) || eq(x, y);
    }

    /**
     * lower &lt; val &lt; upper
     */
    public static <T> boolean inOpenInterval(final Comparable<T> val, final T lower, final T upper) {
        return gt(val, lower) && lt(val, upper);
    }

    /**
     * lower &lt; val ≤ upper
     */
    public static <T> boolean inOpenClosedInterval(final Comparable<T> val, final T lower, final T upper) {
        return gt(val, lower) && le(val, upper);
    }

    /**
     * lower ≤ val &lt; upper
     */
    public static <T> boolean inClosedOpenInterval(final Comparable<T> val, final T lower, final T upper) {
        return ge(val, lower) && lt(val, upper);
    }

    /**
     * lower ≤ val ≤ upper
     */
    public static <T> boolean inClosedInterval(final Comparable<T> val, final T lower, final T upper) {
        return ge(val, lower) && le(val, upper);
    }

    /**
     * val &lt; min → min <br/>
     * val &gt; max → max <br/>
     * else val
     */
    @SuppressWarnings("unchecked")
    public static <T> T clamp(final Comparable<T> val, final T min, final T max) {
        if (lt(val, min)) return min;
        if (gt(val, max)) return max;
        return (T)val;
    }
}

