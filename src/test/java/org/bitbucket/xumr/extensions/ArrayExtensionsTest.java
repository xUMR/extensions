package org.bitbucket.xumr.extensions;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.bitbucket.xumr.extensions.ArrayExtensions.*;
import static org.junit.Assert.*;

/**
 * @author umur
 */
public class ArrayExtensionsTest {
    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void linearSearchTest() throws Exception {

    }

    @Test
    public void isNullOrEmptyTest() throws Exception {

        assertTrue(ArrayExtensions.isNullOrEmpty(null));

        assertTrue(ArrayExtensions.isNullOrEmpty(new Object[0]));

        assertTrue(ArrayExtensions.isNullOrEmpty(new Object[]{}));

        assertFalse(ArrayExtensions.isNullOrEmpty(new Object[]{ null }));

        assertFalse(ArrayExtensions.isNullOrEmpty(new Object[1]));
    }

    @Test
    public void forEachTest() throws Exception {

    }

    @Test
    public void mapTest() throws Exception {
        String[] strings = ArrayExtensions.map(new Integer[]{ 1, 2, 3 }, n -> Integer.toString(n), String.class);

        assertEquals("1", strings[0]);
        assertEquals("2", strings[1]);
        assertEquals("3", strings[2]);
    }

    @Test
    public void map1Test() throws Exception {
        String[] strings = ArrayExtensions.map(new String[]{"1", "2", "3"}, (str, i) -> {
            return str + i;
        }, String.class);

        assertEquals("10", strings[0]);
        assertEquals("21", strings[1]);
        assertEquals("32", strings[2]);
    }

    @Test
    public void reduceTest() throws Exception {
        assertEquals(10, reduce(new Integer[] { 1, 2, 3, 4 }, (x, y) -> x + y, 0).intValue());
        assertEquals(24, reduce(new Integer[] { 1, 2, 3, 4 }, (x, y) -> x * y, 1).intValue());
    }

    @Test
    public void any() throws Exception {

    }

    @Test
    public void all() throws Exception {

    }

    @Test
    public void count() throws Exception {

    }

    @Test
    public void asSetTest() throws Exception {

    }

    @Test
    public void singleTest() throws Exception {

    }

    @Test
    public void single1Test() throws Exception {

    }

    @Test
    public void single2Test() throws Exception {

    }

    @Test
    public void randomElementTest() throws Exception {

    }

    @Test
    public void subsequenceTest() throws Exception {
        Integer[] ints = new Integer[] { 1, 2, 3, 4, 5 };

        assertTrue(Arrays.equals(new Integer[]{ 3 }, ArrayExtensions.subsequence(ints, 2, 3)));
        assertTrue(Arrays.equals(new Integer[]{ 3, 4 }, ArrayExtensions.subsequence(ints, 2, 4)));
        assertTrue(Arrays.equals(new Integer[]{ 2, 3 }, ArrayExtensions.subsequence(ints, 1, 3)));
    }
}
