package org.bitbucket.xumr.extensions;

import jdk.nashorn.internal.objects.annotations.Setter;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.time.Instant;
import java.time.Year;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * @author umur
 */
public class ObjectExtensionsTest {

    private Person p1;
    private int age;

    @Before
    public void setup() {
        age = Year.now().getValue() - 1701;

        p1 = new Person();
        p1.setAge(age);
        p1.setName("Old");
        p1.setSurname("Guy");
        p1.setDeleted(false);
        p1.setDeletedBoxed(false);
    }

    @Test
    public void properties() throws Exception {

        Map<String, Object> properties = ObjectExtensions.properties(p1);
        assertEquals("Old", properties.get("name"));
        assertEquals("Guy", properties.get("surname"));
        assertEquals(age, properties.get("age"));

        System.out.println("ObjExtTest.properties passes");
    }

    @Test
    public void populate() throws Exception {
        Person p2 = new Person();
        Map<String, Object> properties = ObjectExtensions.properties(p1);

        ObjectExtensions.populate(properties, p2);

        assertEquals("Old", p2.getName());
        assertEquals("Guy", p2.getSurname());
        assertEquals(age, p2.getAge());
        assertEquals(false, p2.isDeleted());
        assertEquals(false, p2.getDeletedBoxed());

        properties.put("age", Integer.toString(age));
        properties.put("deletedBoxed", null);

        ObjectExtensions.populate(properties, p2);

        assertEquals("Old", p2.getName());
        assertEquals("Guy", p2.getSurname());
        assertEquals(age, p2.getAge());
        assertEquals(false, p2.isDeleted());
        assertEquals(null, p2.getDeletedBoxed());

        System.out.println("ObjExtTest.populate passes");
    }

    private static class Person {
        private String name;
        private String surname;
        private int age;
        private boolean deleted;
        private Boolean deletedBoxed;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        public Boolean getDeletedBoxed() {
            return deletedBoxed;
        }

        public void setDeletedBoxed(Boolean deletedBoxed) {
            this.deletedBoxed = deletedBoxed;
        }
    }
}