package org.bitbucket.xumr.extensions;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.bitbucket.xumr.extensions.ComparableExtensions.*;
import static org.junit.Assert.*;

/**
 * @author umur
 */
public class ComparableExtensionsTest {

    private BigDecimal[] bigDecimals;

    @Before
    public void setUp() throws Exception {
        int length = 10;

        bigDecimals = new BigDecimal[length];

        for (int i = 0; i < length; i++) {
            bigDecimals[i] = BigDecimal.valueOf(i);
        }
    }

    @Test
    public void ltTest() throws Exception {
        assertTrue(lt(0, 1));
        assertTrue(lt(0.0, 1.0));
        assertTrue(lt("0", "1"));
        assertTrue(lt(bigDecimals[0], bigDecimals[1]));

        assertFalse(lt(0, 0));
        assertFalse(lt(0.0, 0.0));
        assertFalse(lt("0", "0"));
        assertFalse(lt(bigDecimals[0], bigDecimals[0]));

        assertFalse(lt(1, 0));
        assertFalse(lt(1.0, 0.0));
        assertFalse(lt("1", "0"));
        assertFalse(lt(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void gtTest() throws Exception {
        assertFalse(gt(0, 1));
        assertFalse(gt(0.0, 1.0));
        assertFalse(gt("0", "1"));
        assertFalse(gt(bigDecimals[0], bigDecimals[1]));

        assertFalse(gt(0, 0));
        assertFalse(gt(0.0, 0.0));
        assertFalse(gt("0", "0"));
        assertFalse(gt(bigDecimals[0], bigDecimals[0]));

        assertTrue(gt(1, 0));
        assertTrue(gt(1.0, 0.0));
        assertTrue(gt("1", "0"));
        assertTrue(gt(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void eqTest() throws Exception {
        assertFalse(eq(0, 1));
        assertFalse(eq(0.0, 1.0));
        assertFalse(eq("0", "1"));
        assertFalse(eq(bigDecimals[0], bigDecimals[1]));

        assertTrue(eq(0, 0));
        assertTrue(eq(0.0, 0.0));
        assertTrue(eq("0", "0"));
        assertTrue(eq(bigDecimals[0], bigDecimals[0]));

        assertFalse(eq(1, 0));
        assertFalse(eq(1.0, 0.0));
        assertFalse(eq("1", "0"));
        assertFalse(eq(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void neTest() throws Exception {
        assertTrue(ne(0, 1));
        assertTrue(ne(0.0, 1.0));
        assertTrue(ne("0", "1"));
        assertTrue(ne(bigDecimals[0], bigDecimals[1]));

        assertFalse(ne(0, 0));
        assertFalse(ne(0.0, 0.0));
        assertFalse(ne("0", "0"));
        assertFalse(ne(bigDecimals[0], bigDecimals[0]));

        assertTrue(ne(1, 0));
        assertTrue(ne(1.0, 0.0));
        assertTrue(ne("1", "0"));
        assertTrue(ne(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void leTest() throws Exception {
        assertTrue(le(0, 1));
        assertTrue(le(0.0, 1.0));
        assertTrue(le("0", "1"));
        assertTrue(le(bigDecimals[0], bigDecimals[1]));

        assertTrue(le(0, 0));
        assertTrue(le(0.0, 0.0));
        assertTrue(le("0", "0"));
        assertTrue(le(bigDecimals[0], bigDecimals[0]));

        assertFalse(le(1, 0));
        assertFalse(le(1.0, 0.0));
        assertFalse(le("1", "0"));
        assertFalse(le(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void geTest() throws Exception {
        assertFalse(ge(0, 1));
        assertFalse(ge(0.0, 1.0));
        assertFalse(ge("0", "1"));
        assertFalse(ge(bigDecimals[0], bigDecimals[1]));

        assertTrue(ge(0, 0));
        assertTrue(ge(0.0, 0.0));
        assertTrue(ge("0", "0"));
        assertTrue(ge(bigDecimals[0], bigDecimals[0]));

        assertTrue(ge(1, 0));
        assertTrue(ge(1.0, 0.0));
        assertTrue(ge("1", "0"));
        assertTrue(ge(bigDecimals[1], bigDecimals[0]));
    }

    @Test
    public void inOpenIntervalTest() throws Exception {
        assertFalse(inOpenInterval(1, 2, 4));
        assertFalse(inOpenInterval(2, 2, 4));
        assertTrue(inOpenInterval(3, 2, 4));
        assertFalse(inOpenInterval(4, 2, 4));
        assertFalse(inOpenInterval(5, 2, 4));

        assertFalse(inOpenInterval(1.0, 2.0, 4.0));
        assertFalse(inOpenInterval(2.0, 2.0, 4.0));
        assertTrue(inOpenInterval(3.0, 2.0, 4.0));
        assertFalse(inOpenInterval(4.0, 2.0, 4.0));
        assertFalse(inOpenInterval(5.0, 2.0, 4.0));

        assertFalse(inOpenInterval("1", "2", "4"));
        assertFalse(inOpenInterval("2", "2", "4"));
        assertTrue(inOpenInterval("3", "2", "4"));
        assertFalse(inOpenInterval("4", "2", "4"));
        assertFalse(inOpenInterval("5", "2", "4"));

        assertFalse(inOpenInterval(bigDecimals[1], bigDecimals[2], bigDecimals[4]));
        assertFalse(inOpenInterval(bigDecimals[2], bigDecimals[2], bigDecimals[4]));
        assertTrue(inOpenInterval(bigDecimals[3], bigDecimals[2], bigDecimals[4]));
        assertFalse(inOpenInterval(bigDecimals[4], bigDecimals[2], bigDecimals[4]));
        assertFalse(inOpenInterval(bigDecimals[5], bigDecimals[2], bigDecimals[4]));
    }

    @Test
    public void inOpenClosedIntervalTest() throws Exception {
        assertFalse(inOpenClosedInterval(1, 2, 4));
        assertFalse(inOpenClosedInterval(2, 2, 4));
        assertTrue(inOpenClosedInterval(3, 2, 4));
        assertTrue(inOpenClosedInterval(4, 2, 4));
        assertFalse(inOpenClosedInterval(5, 2, 4));

        assertFalse(inOpenClosedInterval(1.0, 2.0, 4.0));
        assertFalse(inOpenClosedInterval(2.0, 2.0, 4.0));
        assertTrue(inOpenClosedInterval(3.0, 2.0, 4.0));
        assertTrue(inOpenClosedInterval(4.0, 2.0, 4.0));
        assertFalse(inOpenClosedInterval(5.0, 2.0, 4.0));

        assertFalse(inOpenClosedInterval("1", "2", "4"));
        assertFalse(inOpenClosedInterval("2", "2", "4"));
        assertTrue(inOpenClosedInterval("3", "2", "4"));
        assertTrue(inOpenClosedInterval("4", "2", "4"));
        assertFalse(inOpenClosedInterval("5", "2", "4"));

        assertFalse(inOpenClosedInterval(bigDecimals[1], bigDecimals[2], bigDecimals[4]));
        assertFalse(inOpenClosedInterval(bigDecimals[2], bigDecimals[2], bigDecimals[4]));
        assertTrue(inOpenClosedInterval(bigDecimals[3], bigDecimals[2], bigDecimals[4]));
        assertTrue(inOpenClosedInterval(bigDecimals[4], bigDecimals[2], bigDecimals[4]));
        assertFalse(inOpenClosedInterval(bigDecimals[5], bigDecimals[2], bigDecimals[4]));
    }

    @Test
    public void inClosedOpenIntervalTest() throws Exception {
        assertFalse(inClosedOpenInterval(1, 2, 4));
        assertTrue(inClosedOpenInterval(2, 2, 4));
        assertTrue(inClosedOpenInterval(3, 2, 4));
        assertFalse(inClosedOpenInterval(4, 2, 4));
        assertFalse(inClosedOpenInterval(5, 2, 4));

        assertFalse(inClosedOpenInterval(1.0, 2.0, 4.0));
        assertTrue(inClosedOpenInterval(2.0, 2.0, 4.0));
        assertTrue(inClosedOpenInterval(3.0, 2.0, 4.0));
        assertFalse(inClosedOpenInterval(4.0, 2.0, 4.0));
        assertFalse(inClosedOpenInterval(5.0, 2.0, 4.0));

        assertFalse(inClosedOpenInterval("1", "2", "4"));
        assertTrue(inClosedOpenInterval("2", "2", "4"));
        assertTrue(inClosedOpenInterval("3", "2", "4"));
        assertFalse(inClosedOpenInterval("4", "2", "4"));
        assertFalse(inClosedOpenInterval("5", "2", "4"));

        assertFalse(inClosedOpenInterval(bigDecimals[1], bigDecimals[2], bigDecimals[4]));
        assertTrue(inClosedOpenInterval(bigDecimals[2], bigDecimals[2], bigDecimals[4]));
        assertTrue(inClosedOpenInterval(bigDecimals[3], bigDecimals[2], bigDecimals[4]));
        assertFalse(inClosedOpenInterval(bigDecimals[4], bigDecimals[2], bigDecimals[4]));
        assertFalse(inClosedOpenInterval(bigDecimals[5], bigDecimals[2], bigDecimals[4]));
    }

    @Test
    public void inClosedIntervalTest() throws Exception {
        assertFalse(inClosedInterval(1, 2, 4));
        assertTrue(inClosedInterval(2, 2, 4));
        assertTrue(inClosedInterval(3, 2, 4));
        assertTrue(inClosedInterval(4, 2, 4));
        assertFalse(inClosedInterval(5, 2, 4));

        assertFalse(inClosedInterval(1.0, 2.0, 4.0));
        assertTrue(inClosedInterval(2.0, 2.0, 4.0));
        assertTrue(inClosedInterval(3.0, 2.0, 4.0));
        assertTrue(inClosedInterval(4.0, 2.0, 4.0));
        assertFalse(inClosedInterval(5.0, 2.0, 4.0));

        assertFalse(inClosedInterval("1", "2", "4"));
        assertTrue(inClosedInterval("2", "2", "4"));
        assertTrue(inClosedInterval("3", "2", "4"));
        assertTrue(inClosedInterval("4", "2", "4"));
        assertFalse(inClosedInterval("5", "2", "4"));

        assertFalse(inClosedInterval(bigDecimals[1], bigDecimals[2], bigDecimals[4]));
        assertTrue(inClosedInterval(bigDecimals[2], bigDecimals[2], bigDecimals[4]));
        assertTrue(inClosedInterval(bigDecimals[3], bigDecimals[2], bigDecimals[4]));
        assertTrue(inClosedInterval(bigDecimals[4], bigDecimals[2], bigDecimals[4]));
        assertFalse(inClosedInterval(bigDecimals[5], bigDecimals[2], bigDecimals[4]));
    }

    @Test
    public void clampTest() throws Exception {
        assertEquals(2, (int)clamp(1, 2, 4));
        assertEquals(2, (int)clamp(2, 2, 4));
        assertEquals(3, (int)clamp(3, 2, 4));
        assertEquals(4, (int)clamp(4, 2, 4));
        assertEquals(4, (int)clamp(5, 2, 4));

        assertEquals(2.0, clamp(1.0, 2.0, 4.0), Double.MIN_VALUE);
        assertEquals(2.0, clamp(2.0, 2.0, 4.0), Double.MIN_VALUE);
        assertEquals(3.0, clamp(3.0, 2.0, 4.0), Double.MIN_VALUE);
        assertEquals(4.0, clamp(4.0, 2.0, 4.0), Double.MIN_VALUE);
        assertEquals(4.0, clamp(5.0, 2.0, 4.0), Double.MIN_VALUE);

        assertEquals("2", clamp("1", "2", "4"));
        assertEquals("2", clamp("2", "2", "4"));
        assertEquals("3", clamp("3", "2", "4"));
        assertEquals("4", clamp("4", "2", "4"));
        assertEquals("4", clamp("5", "2", "4"));

        assertEquals(bigDecimals[2], clamp(bigDecimals[1], bigDecimals[2], bigDecimals[4]));
        assertEquals(bigDecimals[2], clamp(bigDecimals[2], bigDecimals[2], bigDecimals[4]));
        assertEquals(bigDecimals[3], clamp(bigDecimals[3], bigDecimals[2], bigDecimals[4]));
        assertEquals(bigDecimals[4], clamp(bigDecimals[4], bigDecimals[2], bigDecimals[4]));
        assertEquals(bigDecimals[4], clamp(bigDecimals[5], bigDecimals[2], bigDecimals[4]));
    }
}
