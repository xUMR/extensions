A collection of static methods. To add this project as a dependency, add the following to pom.xml file:
```
<repositories>
    <repository>
        <id>org.bitbucket.xumr.extensions</id>
        <url>https://dl.dropboxusercontent.com/u/126445027/mvn</url>
    </repository>
</repositories>

<dependencies>
    ...
    <dependency>
        <groupId>org.bitbucket.xumr</groupId>
        <artifactId>extensions</artifactId>
        <version>0.4.10</version>
    </dependency>
    ...
</dependencies>
```

## License
[MIT LICENSE](LICENSE.md)
